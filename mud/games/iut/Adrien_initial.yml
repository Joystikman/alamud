---
id: ile-007
type: Location
contains:
  - piolet-000
events:
  info:
    actor: "Ile fantastique"
  look:
     actor: |
        Vous êtes sur une petite île, vous sentez comme une force mystérieuse...
        <br>D'ici vous pouvez apercevoir, à travers la brume, la Tour Céleste <b>au Nord</b>.
        <br><b>Au Sud</b>, vous voyez le village de pécheurs.
---
id: piolet-000
type: Thing
name:
  - piolet magique
  - piolet
  - piolet scintillant
props:
  - takable
events:
  info:
    actor: "Un piolet scintillant"
  look:
    actor: "C'est un piolet, il scintille, il doit probablement être magique."
---
id: tour-008
type: Location
contains:
  - armor-000
events:
  info:
    actor: "Tour Céleste"
  look:
    actor: |
       Vous entrez dans la Tour Céleste.
       <br>Elle qui était si rayonnante et pleine de vie il y de cela quelques jours... est maintenant déserte. <i>Que s'est-il passé ici ?</i>
       <br>Le capitaine vous a lâchement abandonné, le bateau ne reviendra pas, aucun retour en arrière n'est possible.
       <br> <b>Au sud-est</b>, vous apercevez votre seule issue, la Grande Plaine.
---
id: armor-000
type: Thing
gender: f-c
name:
  - armure en or
  - armure
  - protection
  - armure d'or
props:
  - takable
events:
  info:
    actor: "Une armure en or"
  look:
    actor: "C'est une armure en or, suffisament résistante pour vous protéger de maintes attaques."
---
id: village-006
type: Location
parts:
  - capitaine-000
events:
  info:
    actor: "Village de pécheurs."
  look:
    actor: |
      Vous vous retrouvez dans ce pittoresque petit village de pécheurs fort accueillant.
      <br>Loin derrière vous, à <b>l'est</b>, se trouve la Grande Plaine.<br>
      {% if the("capitaine-000").has_prop("paid") %}
      <b>Au nord</b> vous voyez le capitaine et son bateau qui vous attendent.
      {% else %}
      <b>Un vieux capitaine</b> avec un cache oeil et un perroquet vous fait signe.
      {% end %}
---
id: porte-tour-009
type: Location
parts:
  - banshee-000
events:
  info:
    actor: "Vous apercevez un être diforme, bien plus impressionnant que celui que vous avez affronté."
  look:
    actor: |
      {% if the("banshee-000").has_prop("alive") %}
      Entre vous et la Grande Plaine <b>au sud est</b> se dresse un monstrueux <b>Banshee</b>, si vous voulez passer, il faudra le tuer.
      {% else %}
      Le cadavre du Banshee gît sur le sol dans une marre de sang noir.
      {% end %}
---
id: banshee-000
type: Thing
name: banshee
props:
  - alive
  - armor-needed
events:
  look:
    actor: |
      {% if the("banshee-000").has_prop("alive") %}
      Le Banshee vous fixe avec ses yeux noir.
      {% else %}
      Le Banshee est mort.
      {% end %}
  attack-with:
    - props:
        - banshee-000:alive
        - object2:key-for-sword-000
        - -banshee-000:fought-without-armor
      effects: &bansheeWinEff
        - type: ChangePropEffect
          modifs:
            - -banshee-000:alive
            - -portal-porte-tour-plaine-000:closed
      actor: &bansheeWinTxt |
        Vous engager un combat sans merci avec le Banshee.
        Les coups fusent de toutes parts, mais votre armure vous protège.
        Le combat tourne en votre faveur.
    - props:
        - -banshee-000:alive
      actor: &bansheeA "Ce banshee mérite bien encore un petit coup d'épée !"
    - actor: &bansheeLoose |
        Le Banshee s'avère plus rusé et plus résistant que vous ne le pensiez.
        Son cri vous glace d'effroi, <b>ce moment d'innatention vous est fatal</b>.
      effects:
        - type: DeathEffect
  attack:
    failed:
      actor: *bansheeLoose
      effects:
        - type: DeathEffect
    armor-with-simple:
      effects: *bansheeWinEff
      actor: *bansheeWinTxt
    acharnement:
      actor: *bansheeA
---
id: vallee-013
type: Location
parts:
  - balrog-000
events:
  info:
    actor: "Vous arrivez dans une étroite vallée."
  look:
    actor: |
      {% if the("balrog-000").has_prop("alive") %}
      Au bout de la vallée, vers le <b>nord</b>, vous apercevez un terrifiant <b>Balrog</b> qui vous barre le passage.
      <b>Au sud</b> se trouve la Grande Plaine.
      Il vous barre le passage, si vous désirez passer, vous serez contraint de le tuer.
      {% else %}
      La voie est libre, le corps du Balrog git sur le sol.
      {% end %}
---
id: balrog-000
type: Thing
name: balrog
props:
  - alive
  - armor-needed
events:
  look:
    actor: |
      {% if the("balrog-000").has_prop("alive") %}
      La chaîne du Balrog claque sur le sol, un seul coup et vous rejoindrez les mines de la Moria.
      {% else %}
      Le corps ensanglanté du Balrog est marqué d'une multitude de coups d'épée.
      {% end %}
  attack-with:
    - props:
        - balrog-000:alive
        - object2:key-for-sword-001
        - -balrog-000:fought-without-armor
      effects: &balrogWinEff
        - type: ChangePropEffect
          modifs:
            - -balrog-000:alive
            - -portal-vallee-terre-desolee-000:closed
      actor: &balrogWinTxt |
        Vous esquivez habillement la chaine du Balrog. Votre épée en or vous permet de briser sans grandes difficultés son épaisse armure.
        Ce long combat vous affaiblit de plus en plus.
        <br>Votre fin semble proche.
        <br>Dans un dernier sursaut de puissance, vous assenez un coup remarquable dans le dos du Balrog.
        <b>Ce dernier lui est fatal, vous êtes victorieux.</b>
    - props:
        - -balrog-000:alive
      actor: &balrogA "Toute cette colère !! Il faut apprendre à la canaliser. Le combat final approche."
    - actor: &balrogLoose |
        Le combat s'engage, le Balrog vous domine.
        Un coup bien placé vous fait chuter, le Balrog profite de cet instant pour broyer votre corps de tout son poids.
      effects:
        - type: DeathEffect
  attack:
    armor-with-golden: 
      actor: *balrogWinTxt
      effects: *balrogWinEff
    failed:
      actor: *balrogLoose
      effects:
        - type: DeathEffect
    acharnement:
      actor: *balrogA
---
id: terre-desolee-014
type: Location
parts:
  - titan-000
events:
  info:
    actor: "L'air est irrespirable"
  look:
    actor: |
      {% if the("titan-000").has_prop("alive") %}
      La fummée qui vous entoure rend l'air irrespirable. Sous vos pieds, le sol est brulant.
      Alors que vous tentez de vous repérer, vous apercevez un <b>Titan</b>, une créature aux pouvoirs incommensurables.
      <b>Au sud</b> vous apercevez difficilement la vallée.
      <br>Vous êtes face à votre destin.
      {% else %}
      Vous êtes un héros, vous avez sauvé le royaume.
      {% end %}
---
id: titan-000
type: Thing
name: titan
props:
  - alive
  - armor-needed
  - orbe-needed
events:
  look:
    actor: |
      {% if the("titan-000").has_prop("alive") %}
      Le Titan vous regarde de ses 100m de haut. Vous êtes n'êtes pas effrayé, le coeur empli de haine, vous savez que c'est bientôt la fin.
      {% else %}
      Vous n'en croyez pas vos yeux. Une créature aussi gigantesque qu'un Titan ne vous a pas résisté, vous l'avez bel et bien tué.
      {% end %}
  attack-with:
    - props:
        - titan-000:alive
        - object2:key-for-sword-001
        - -titan-000:fought-without-armor
        - -titan-000:fought-without-orbe
      effects: &titanWinEff
        - type: ChangePropEffect
          modifs:
            - -titan-000:alive
      actor: &titanWinTxt |
        Vous vous accrochez habilement au Titan, vous parvenez à gravir son dos malgré ses divers mouvements.
        <br>Vous ateignez bientôt sa tête.
        <br>Vous lui assénez une multitude de coups au crâne qui auraient du s'avérer fatals.
        Malheureusement pour vous, le Titan régénère très rapidement, trop rapidement, les coups que vous lui assénez sont inefficaces.
        <br>Vous utilisez donc <b>l'Orbe de Crystal</b> que vous avez trouver dans la grotte.
        <br>Dès lors, la force du Titan semble décroitre, il est moins vif.
        <br>Vous profitez de ce moment de faiblesse pour planter votre épée au milieu de son crâne.
        <br>Le Titan commence à chanceler, et chûte.
        <br>Le choc est violent, vous êtes sérieusement sonné.
        <br>Lorsque vous reprenez vos esprit, vous réalisez que le Titan est mort.
        <br><h1 style="text-align: center;">Vous avez gagné !</h1>
    - props:
        - -titan-000:alive
      actor: &TitanA "Le Titan est déjà mort, à votre tour."
      effects:
        - type: DeathEffect
    - actor: &titanLoose |
        Vous gravissez le dos du Titan, en atteignant sa tête, vous utilisez votre épée pour le frapper.
        <br>Sa force colossale, met votre endurance au combat à rude épreuve.
        <br>Vous faites une chûte mais pas mortelle.
        <br>Le Titan se penche vers vous, vous saisit dans une de ses mains et vous serre, <b>jusqu'à broyer votre cage thoracique</b>.
        <br>Votre vision se brouille, vous ne voyez plus la lumière.
      effects:
        - type: DeathEffect
  attack:
    armor-with-golden:
      actor: *titanWinTxt
      effects: *titanWinEff
    acharnement:
      actor: *TitanA
      effects:
        - type: DeathEffect
    failed:
      actor: *titanLoose
      effects:
        - type: DeathEffect
---
id: portal-village-ile-000
type: Portal
props: 
  - closed
exits:
  - id: village-006-nord
    location: village-006
    direction: nord
    events:
      enter-portal:
        actor: "Vous montez dans le bateau en compagnie du capitaine et partez en direction de cette mystérieuse île."
  - id: ile-007-sud
    location: ile-007
    direction: sud
    events:
      enter-portal:
        actor: "Vous repartez vers le bateau pour retournez au village."
---
id: portal-ile-tour-000
type: Portal
exits:
  - id: ile-007-nord
    location: ile-007
    direction: nord
    events:
      enter-portal:
        actor: "Vous monter dans un bateau en direction de la Tour Céleste que vous avez aperçue dans la brume... Le capitaine a l'air de plus en plus inquiet."
  - id: tour-008-sud
    location: tour-008
    direction:
    events:
      enter-portal:
        failed:
          actor: "Vous ne pouvez pas repartir, le capitaine, effrayé par la situation, vous a abandonné."
---
id: portal-tour-porte-tour-000
type: Portal
exits:
  - id: tour-008-est
    location: tour-008
    direction: sud-est
    events:
      traversal:
        - props: banshee-000:alive
          exit1: =tour-008-est
          exit2: =porte-tour-009-ouest
        - exit1: =tour-008-est
          exit2: =plaine-004-nord-ouest
      enter-portal:
        actor: "Vous vous dirigez vers la sortie de la Tour Céleste, direction la Grande Plaine."
  - id: porte-tour-009-ouest
    location: porte-tour-009
    direction: nord-ouest
    events:
      enter-portal:
        actor: "Vous fuyez lâchement face au Banshee."
---
id: portal-plaine-village-000
type: Portal
exits:
  - id: plaine-004-ouest
    location: plaine-004
    direction: ouest
    events:
      enter-portal:
        actor: "Vous suivez le long chemin en pierres qui conduit au village."
  - id: village-006-est
    location: village-006
    direction: est
    events:
      enter-portal:
        actor: "Vous retournez vers la Grande Plaine."
---
id: portal-porte-tour-plaine-000
type: Portal
props:
  - closed
exits:
  - id: porte-tour-009-sud-est
    location: porte-tour-009
    direction: sud-est
    events:
      enter-portal:
        failed: 
          actor: "Le Banshee garde le passage, impossible de passer sans le combattre."
        actor: "Maintenant que le Banshee est mort, la route est libre, vous pouvez retourner vers la Grande plaine."
  - id: plaine-004-nord-ouest
    location: plaine-004
    direction: nord-ouest
    events:
      traversal:
        - props: banshee-000:alive
          exit1: =plaine-004-nord-ouest
          exit2: =porte-tour-009-sud-est
        - exit1: =plaine-004-nord-ouest
          exit2: =tour-008-est
      enter-portal:
        - props: banshee-000:alive
          actor: "Alors que vous vous dirigiez vers la Tour céleste, vous êtes attaqué par surprise par un Banshee, votre épée seule ne suffit pas a vous défendre contre le Banshee, il vous aurait fallu une armure."
          data-driven: true
          effects:
            - type: DeathEffect
        - actor: "Vous retournez à la tour céleste."
---
id: portal-plaine-vallee-000
type: Portal
exits:
  - id: plaine-004-nord
    location: plaine-004
    direction: nord
    events:
      enter-portal:
        actor: "Vous vous dirigez vers une sombre vallée."
  - id: vallee-013-sud
    location: vallee-013
    direction: sud
    events:
      enter-portal:
        actor: "Vous ne vous sentez pas près pour affronter le Balrog, vous faites demi-tour."
---
id: portal-vallee-terre-desolee-000
type: Portal
props:
  - closed
exits:
  - id: vallee-013-nord
    location: vallee-013
    direction: nord
    events:
      enter-portal:
        failed:
          actor: "Un terrifiant Balrog vous barre la route. Un combat est inévitable pour continuer."
        actor: "Le Balrog est mort, la voie est libre"
  - id: terre-desolee-014-sud
    location: terre-desolee-014
    direction: sud
    events:
      enter-portal:
        actor: |
          {% if the("titan-000").has_prop("alive") %}
          Tôt au tard, il faut savoir affronter son destin.
          {% else %}
          Vous êtes aclamé par l'ensemble des peuples du royaumes.
          {% end %}
---