# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from mud.actions import (
    GoAction, TakeAction, LookAction, InspectAction, OpenAction,
    OpenWithAction, CloseAction, InventoryAction,
    LightOnAction, LightOffAction, DropAction, DropInAction,
    PushAction, TeleportAction, EnterAction, LeaveAction, AttackWithAction,AttackAction, PayWithAction, PayAction,CmdListAction,
)

import mud.game

def make_rules():
    GAME = mud.game.GAME
    DIRS = list(GAME.static["directions"]["noun_at_the"].values())
    DIRS.extend(GAME.static["directions"]["noun_the"].values())
    DIRS.extend(GAME.static["directions"]["normalized"].keys())
    DETS = "(?:l |le |la |les |une |un |)"

    return (
        (GoAction       , r"(?:aller |)(%s)" % "|".join(DIRS)),
        (TakeAction     , r"(?:p|prendre) %s(\D+)" % DETS),
        (LookAction     , r"(?:r|regarder)"),
        (InspectAction  , r"(?:r|regarder|lire|inspecter|observer|parler) %s(\D+)" % DETS),
        (OpenAction     , r"ouvrir %s(\w+)" % DETS),
        (OpenWithAction , r"ouvrir %s(\w+) avec %s(\w+)" % (DETS,DETS)),
        (CloseAction    , r"fermer %s(\w+)" % DETS),
        (InventoryAction, r"(?:inventaire|inv|i)$"),
        (LightOnAction  , r"allumer %s(\w+)" % DETS),
        (LightOffAction , r"[eé]teindre %s(\w+)" % DETS),
        (DropAction     , r"(?:poser|laisser) %s(\w+)" % DETS),
        (DropInAction   , r"(?:poser|laisser) %s(\w+) (?:dans |sur |)%s(\w+)" % (DETS,DETS)),
        (PushAction     , r"(?:appuyer|pousser|presser)(?: sur|) %s(\w+)" % DETS),
        (TeleportAction , r"tele(?:porter|) (\S+)"),
        (EnterAction    , r"entrer"),
        (LeaveAction    , r"sortir|partir"),
        (AttackWithAction , r"(?:attaquer|tuer|charger|taper|d[eé]truire) %s(\w+) avec %s(\D+)" % (DETS,DETS)),
        (AttackAction , r"(?:attaquer|taper|tuer|charger|d[eé]truire) %s(\w+)" % DETS),
        (PayWithAction, r"payer %s(\w+) avec %s(\D+)" % (DETS,DETS)),
        (PayAction, r"payer %s(\w+)" % DETS),
		(CmdListAction, r"(?:cmd|commande|commandes|help|aide)"),
    )
