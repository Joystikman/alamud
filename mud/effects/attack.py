from .effect import Effect2, Effect3
from mud.events import AttackWithEvent, AttackEvent

class AttackWithEffect(Effect3):
    EVENT = AttackWithEvent

class AttackEffect(Effect2):
    EVENT = AttackEvent