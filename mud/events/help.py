from .event import Event1

class CmdListEvent(Event1):
	NAME = "list-cmd"
	
	def perform(self):
		self.inform("list-cmd")