from .event import Event2, Event3

class PayEvent(Event2):
    NAME = "pay.have-paid"
    def perform(self):
        if self.object.has_prop("paid"):
            self.object.remove_prop("paid")
        if "or-000" not in [nom.id for nom in self.actor.contents()]:
            self.fail()
            return self.inform("pay.failed")
        self.inform("pay.have-paid")


class PayWithEvent(Event3):
    NAME = "pay-with.have-paid"
    def perform(self):
        if self.object.has_prop("paid"):
            self.object.remove_prop("paid")
        if self.object2.id != "or-000":
            self.fail()
            return self.inform("pay-with.failed")
        self.inform("pay-with.have-paid")
