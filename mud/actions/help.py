from .action import Action1
from mud.events import CmdListEvent

class CmdListAction(Action1):
    EVENT = CmdListEvent
    ACTION = "list-cmd"
