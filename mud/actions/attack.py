from .action import Action2, Action3
from mud.events import AttackWithEvent, AttackEvent

class AttackWithAction(Action3):
    EVENT = AttackWithEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
    ACTION = "attack-with"
    
class AttackAction(Action2):
    EVENT = AttackEvent
    RESOLVE_OBJECT = "resolve_for_operate"
    ACTION = "attack"